<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');            
            $table->unsignedBigInteger('cinema_id');            
            $table->unsignedBigInteger('theatre_id');            
            $table->unsignedBigInteger('movie_id');            
            $table->unsignedBigInteger('show_time_id');            
            $table->string('reference');
            $table->datetime('show_date', $precision = 0);
            $table->integer('number_of_seats');
            $table->decimal('total_cost');
            $table->enum('status',['booked','cancelled'])->default('booked');
            $table->timestamps();            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('theatre_id')->references('id')->on('theatres');
            $table->foreign('cinema_id')->references('id')->on('cinemas');
            $table->foreign('movie_id')->references('id')->on('movies');
            $table->foreign('show_time_id')->references('id')->on('schedules');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
