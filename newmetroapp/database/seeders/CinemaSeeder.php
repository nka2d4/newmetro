<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CinemaSeeder extends Seeder
{

    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        
        // Insert Cavendish Cinema        
        DB::table('cinemas')->insert([
            'name' => 'Cavendish Square',                        
        ]);

        
        // Insert Canal Walk Cinema        
        DB::table('cinemas')->insert([
            'name' => 'Canal Walk',                        
        ]);
    }
}
