<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	// create a new config entry in db table
    	$config = new Config;

    	// set key value columns
    	$config->key = 'max_number_seats';
    	$config->value = 30;

    	// save entry in db
    	$config->save();

    	// create a new config entry in db table
    	$config = new Config;

    	// set key value columns
    	$config->key = 'tmdb_image_base_url';
    	$config->value = 'https://api.themoviedb.org/3/movie/now_playing';

    	// save entry in db
    	$config->save();

        // create a new config entry in db table
        $config = new Config;

        // set key value columns
        $config->key = 'ticket_price';
        $config->value = 95;

        // save entry in db
        $config->save();
    }
}
