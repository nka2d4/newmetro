<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Theatre;
use App\Models\Cinema;
use App\Models\Movie;
use App\Models\CinemaSchedule;

class TheatreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// fetch cinemas
        $cinemas = Cinema::all();

        // set number of theatres
        $theatreCount = 2;

        // set a movie id, assuming movie ids start from 1 and
        // is available in the db.
        $movieId = 1;
    		
		// loop through each
		foreach ($cinemas as $cinema) {

			// loop through each theatre
			for ($i=0; $i < $theatreCount; $i++) {
				
				// create a new Theatre entry in db table
				$theatre = new Theatre;

				// set fields       	
				$theatre->name = "Cine ". ($i+1);
				$theatre->cinema_id = $cinema->id;
				$theatre->movie_id = $movieId;  

				// set next movie id
				++$movieId;      	

				// save entry in db
				$theatre->save();
			}

		}
        
    }
}
