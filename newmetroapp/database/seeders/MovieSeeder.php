<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Cinema;
use App\Models\Movie;
use App\Services\MovieGateway;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// fetch cinemas
        $cinemas = Cinema::all();

        // instantiate movie gateway object
        $movieGateway = new MovieGateway();        

        // fetch latest movie list from tmdb API
        $nowPlaying = $movieGateway->fetchMovieList();
        
    	// loop through movie info
        foreach ($nowPlaying['results'] as $movie){

	        // Insert movie		        
	        DB::table('movies')->insert([		            
	            'title' => $movie['title'],
	            'rating' => $movie['vote_average'],
	            'poster_path' => $movie['poster_path'],		            
	            'release_date' => $movie['release_date'],
	        ]);
        }       
        
    }
}
