<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Schedule;
use App\Models\Theatre;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // fetch all theatres 
        $theatres = Theatre::all();

        // set show time array
        $showTimes = ['10:00','13:00','16:00','20:00','23:00'];

        // loop through each theatre
        foreach ($theatres as $theatre) {

            // loop through each show time
    	   foreach ($showTimes as $time) {

            	// create a new schedule entry in db table
            	$schedule = new Schedule;

            	// set show time        	
            	$schedule->show_time = $time;

                // set theatre id
                $schedule->theatre_id = $theatre->id;

            	// save entry in db
            	$schedule->save();
        	}
        }
        	     	
        
    }
}
