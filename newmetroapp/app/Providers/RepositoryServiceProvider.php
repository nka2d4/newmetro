<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\CinemaRepository;
use App\Repositories\Interfaces\CinemaRepositoryInterface;

use App\Repositories\BookingRepository;
use App\Repositories\Interfaces\BookingRepositoryInterface;

use App\Repositories\MovieRepository;
use App\Repositories\Interfaces\MovieRepositoryInterface;

use App\Repositories\ScheduleRepository;
use App\Repositories\Interfaces\ScheduleRepositoryInterface;

use App\Repositories\TheatreRepository;
use App\Repositories\Interfaces\TheatreRepositoryInterface;

use App\Repositories\ConfigRepository;
use App\Repositories\Interfaces\ConfigRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CinemaRepositoryInterface::class, CinemaRepository::class);
        $this->app->bind(BookingRepositoryInterface::class, BookingRepository::class);
        $this->app->bind(MovieRepositoryInterface::class, MovieRepository::class);
        $this->app->bind(ScheduleRepositoryInterface::class, ScheduleRepository::class);
        $this->app->bind(TheatreRepositoryInterface::class, TheatreRepository::class);
        $this->app->bind(ConfigRepositoryInterface::class, ConfigRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
