<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Booking;
use App\Models\Config;
use App\Models\CinemaSchedule;

class Booking extends Model
{
    use HasFactory;
}
