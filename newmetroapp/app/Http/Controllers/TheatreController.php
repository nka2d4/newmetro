<?php

namespace App\Http\Controllers;

use App\Models\Theatre;
use App\Models\Movie;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\TheatreRepositoryInterface;
use App\Repositories\Interfaces\MovieRepositoryInterface;

class TheatreController extends Controller
{
    private $theatreRepositoryInterface;

    /**
     * Constructor for controller
     *
     * @return void
     */
    public function __construct(
        TheatreRepositoryInterface $theatreRepositoryInterface,
        MovieRepositoryInterface $movieRepositoryInterface,
    )
    {
        $this->theatreRepositoryInterface = $theatreRepositoryInterface;
        $this->movieRepositoryInterface = $movieRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get theatre list
        $theatres = $this->theatreRepositoryInterface->list();        

        return view('crud.theatre.list', ['theatres'=>$theatres]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function show(Theatre $theatre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function edit(Theatre $theatre)
    {
        $info = $this->theatreRepositoryInterface->info($theatre->id);

        $movies = $this->movieRepositoryInterface->get();


        // load cancel a booking page
        return view('crud.theatre.edit', ['theatreInfo'=>$info, 'movies'=>$movies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Theatre $theatre)
    {
        // update theatre movie
        $status = $this->theatreRepositoryInterface->save(
            $theatre->id,
            $request->input('movie_id')
        );
        
        // redirect to theatre list
        return redirect('theatre')->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theatre $theatre)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function id(int $movieId, $cinemaId)
    {
        // get theatre id selected
        $theatreId = $this->theatreRepositoryInterface->id(
            $movieId,
            $cinemaId
        );

        return ['theatre_id'=>$theatreId];
    }

}
