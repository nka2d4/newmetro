<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Cinema;
use App\Models\Movie;
use App\Models\Schedule;
use App\Models\Config;
use App\Models\User;
use App\Models\Theatre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Repositories\Interfaces\CinemaRepositoryInterface;
use App\Repositories\Interfaces\BookingRepositoryInterface;
use App\Repositories\Interfaces\MovieRepositoryInterface;
use App\Repositories\Interfaces\ScheduleRepositoryInterface;
use App\Repositories\Interfaces\TheatreRepositoryInterface;
use App\Repositories\Interfaces\ConfigRepositoryInterface;

class BookingController extends Controller
{

    private $cinemaRepositoryInterface;
    private $bookingRepositoryInterface;
    private $movieRepositoryInterface;
    private $scheduleRepositoryInterface;
    private $theatreRepositoryInterface;
    private $configRepositoryInterface;

    /**
     * Constructor for controller
     *
     * @return void
     */
    public function __construct(
        CinemaRepositoryInterface $cinemaRepositoryInterface,
        BookingRepositoryInterface $bookingRepositoryInterface,
        MovieRepositoryInterface $movieRepositoryInterface,
        ScheduleRepositoryInterface $scheduleRepositoryInterface,
        TheatreRepositoryInterface $theatreRepositoryInterface,
        ConfigRepositoryInterface $configRepositoryInterface,
    )
    {
        // set properties
        $this->cinemaRepositoryInterface = $cinemaRepositoryInterface;
        $this->bookingRepositoryInterface = $bookingRepositoryInterface;
        $this->movieRepositoryInterface = $movieRepositoryInterface;
        $this->scheduleRepositoryInterface = $scheduleRepositoryInterface;
        $this->theatreRepositoryInterface = $theatreRepositoryInterface;
        $this->configRepositoryInterface = $configRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // get booking list
        $bookings = $this->bookingRepositoryInterface->list(Auth::user());        

        return view('booking.list', ['bookings'=>$bookings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // fetch cinemas
        $cinemas = $this->cinemaRepositoryInterface->get();
        
        // return view
        return view('booking.create', ['cinemas'=>$cinemas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {           
        // redirect user if not logged in.
        // having issues with middleware
        if (!Auth::check()) 
            return redirect('/login');

        // get the show date and time
        $showDateAndTime = $this->scheduleRepositoryInterface->showDateTime(
            $request->input('show_time_id')
        );

        // get the ticket price
        $ticketPrice = $this->configRepositoryInterface->value('ticket_price');

        // set attributes of booking in assoc array
        $attributes = [
            "cinema_id"=>$request->input('cinema_id'),
            "movie_id"=>$request->input('movie_id'),
            "show_time_id"=>$request->input('show_time_id'),
            "theatre_id"=>$request->input('theatre_id'),
            "seats"=>$request->input('seats'),
            "ticket_price"=>$ticketPrice,
            "show_date_and_time"=>$showDateAndTime
        ];

        // place booking
        $status = $this->bookingRepositoryInterface->save($attributes, Auth::user());

        
        // redirect user to dashboard to view order
        if ($status["is_saved"]) {

            // redirect user to the booking dashboard
            return redirect('booking')->with('status', $status);            
        }

        // show the save error if any
        return view('booking.create', ['status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {   
        // check if movie can be cancelled
        $cancelMovie = $this->bookingRepositoryInterface->isCancellationAllowed(
            $booking->id, Auth::user()
        );

        // get booking info for the logged in user
        $bookingInfo = $this->bookingRepositoryInterface->info($booking->id, Auth::user());

        // load cancel a booking page
        return view('booking.edit', ['bookingInfo'=>$bookingInfo,'cancelMovie'=>$cancelMovie]);       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        

        // cancel a booking
        $result = $this->bookingRepositoryInterface->cancel(
            $booking->id,
            Auth::user(),
            $booking
        );
        
        // redirect to booking list if booking saved successfully.
        if ($result['status'])
            return redirect('booking')->with('result', $result);        
        

        // show error message to user
        return view('booking.edit', ['bookingInfo'=>$booking,'result'=>$result]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }

    /**
     * Get remaining seat count
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function remainingseats($movieId, $cinemaId, $showTimeId, $theatreId, $maxNumberSeats)
    {        
        $remainingSeats =$this->bookingRepositoryInterface->remainingSeatCount(
            $movieId, $cinemaId, $showTimeId, $theatreId, $maxNumberSeats
        );

        return ['remaining_seat_count' => $remainingSeats];
    }
    
}
