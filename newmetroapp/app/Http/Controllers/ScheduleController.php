<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\ScheduleRepositoryInterface;
use App\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    private $scheduleRepositoryInterface;

    /**
     * Constructor for controller
     *
     * @return void
     */
    public function __construct(ScheduleRepositoryInterface $scheduleRepositoryInterface,)
    {
        $this->scheduleRepositoryInterface = $scheduleRepositoryInterface; 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CinemaSchedule $cinemaSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(CinemaSchedule $cinemaSchedule)
    {

    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CinemaSchedule $cinemaSchedule
     * @return \Illuminate\Http\Response
     */
    public function list($cinemaId, $movieId)
    {
        // get show times
        return $this->scheduleRepositoryInterface->list(
            $cinemaId, $movieId
        );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CinemaSchedule  $cinemaSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(CinemaSchedule $cinemaSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CinemaSchedule  $cinemaSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CinemaSchedule $cinemaSchedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CinemaSchedule  $cinemaSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(CinemaSchedule $cinemaSchedule)
    {
        //
    }
}
