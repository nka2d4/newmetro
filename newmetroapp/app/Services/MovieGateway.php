<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class MovieGateway
{

    /**
     * Get the the current movie list from the api TMDB.
     *
     * @param  int  $id
     * @return json
     */
    public function fetchMovieList()
    {	    
    	try
    	{
	        // get the now playing movies from tmdb api
	        $options = config('services.tmdb');
	        $options[] = ["page"=>1];

	        return $nowPlaying = Http::
	             get('https://api.themoviedb.org/3/movie/now_playing', $options)
	             ->json();
	        
	    }
	    catch (exception $ex)
	    {
	    	Log::info($ex->message());

	    	return ["error"=>$ex->message()];
	    }
    }
}