<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Interfaces\MovieRepositoryInterface;
use App\Models\Movie;

class MovieRepository implements MovieRepositoryInterface
{
	private $model;

	 /**
     * Constructor for controller
     * @param App\Models\Movie $movie
     * @return void
     */
	public function __construct(Movie $movie)
	{
		$this->model = $movie;
	}

	/**
     * Retrieve the movie list for a cinema
     *
     * @param  int  $cinemaId
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function list(int $cinemaId): Collection
	{
		return $this->model
		->join('theatres','theatres.movie_id','=','movies.id')        
        ->select(
            'theatres.movie_id'
            ,'movies.title', 'movies.poster_path', 'movies.title', 'movies.rating'
        )
        ->where('theatres.cinema_id','=',$cinemaId)
        ->orderBy('theatres.id','asc')
        ->get();
	}

	/**
     * Retrieve the movie list
     *     
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function get(): Collection
	{
		return $this->model->all();
	}
}