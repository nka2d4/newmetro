<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Interfaces\ScheduleRepositoryInterface;
use App\Models\Schedule;

class ScheduleRepository implements ScheduleRepositoryInterface
{
	private $model;

    /**
     * Constructor for controller
     *
     * @param App\Models\Schedule $schedule
     * @return void
     */
	public function __construct(Schedule $schedule)
	{
		$this->model = $schedule;
	}

    /**
     * Retrieve the shedule list
     *
     * @param  int  $cinemaId, int $movieId
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function list(int $cinemaId, int $movieId): Collection
	{
		return $this->model		       
        ->join('theatres','theatres.id','=','schedules.theatre_id')        
        ->select(
            'schedules.show_time' , 'schedules.id'           
        )
        ->where([
            ['theatres.cinema_id','=',$cinemaId]
            , ['theatres.movie_id','=',$movieId]
            , ['schedules.show_time','>',\Carbon\Carbon::now()->toTimeString()]
        ])
        ->orderBy('schedules.id','asc')
        ->get();
	}

    /**
     * Retrieve the show date
     *
     * @param  int  $id
     * @return string
     */
    public static function showDateTime(int $id): string
    {
        // get the movie time
        $showTime = Schedule::find($id);
            
        // create the show time with the date
        return \Carbon\Carbon::now()->format('Y-m-d') .' ' . $showTime->show_time;
    }
}