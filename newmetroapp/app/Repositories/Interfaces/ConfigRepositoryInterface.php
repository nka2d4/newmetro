<?php

namespace App\Repositories\Interfaces;

interface ConfigRepositoryInterface {

    /**
     * Get value from key value pair table
     *
     * @param  string  $key
     * @return string
     */
    public function value(string $key);

}

