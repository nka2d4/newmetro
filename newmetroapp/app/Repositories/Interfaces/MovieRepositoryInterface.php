<?php

namespace App\Repositories\Interfaces;

interface MovieRepositoryInterface {

	/**
     * Retrieve the movie list for a cinema
     *
     * @param  int  $cinemaId
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function list(int $cinemaId);


	/**
     * Retrieve the movie list
     *     
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function get();

}