<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Booking;

interface BookingRepositoryInterface {

	/**
     * Get booking info per user
     *
     * @param  User $user
     * @return int
     */
	public function list(User $user);

	/**
     * Get remaining seat count
     *
     * @param  int $movieId, int $cinemaId, int $showTimeId, int $theatreId, int $maxNumberSeats
     * @return int
     */
	public function remainingSeatCount(
		int $movieId,
		int $cinemaId,
		int $showTimeId,
		int $theatreId,
		int $maxNumberSeats
	);

	/**
     * Test if cancellation is allowed
     *
     * @param  int $id, User $user
     * @return bool
     */
	public function isCancellationAllowed(int $id, User $user);


    /**
     * Save a booking in db
     *
     * @param  int $id, User $user
     * @return App\Models\Booking
     */
	public function info(int $id, User $user);

	/**
     * Save a booking in db
     *
     * @param  int $id, User $user, Booking $booking
     * @return @void
     */
	public function cancel(int $id, User $user, Booking $booking);

	/**
     * Save a booking in db
     *
     * @param  array $attributes, User $user 
     * @return array
     */
	public function save(array $attributes, User $user);

 	/**
     * calculate the total cost for the booking
     *
     * @param  int $seats 
     * @return float
     */
	public function calculateTotalCost(int $seats, float $ticketPrice); 

}