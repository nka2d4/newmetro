<?php

namespace App\Repositories\Interfaces;

interface CinemaRepositoryInterface {

	/**
     * Retrieve the cinema list
     *     
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function get();

}