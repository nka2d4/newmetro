<?php

namespace App\Repositories\Interfaces;

interface TheatreRepositoryInterface {

	/**
     * Get theatre id
     * 
     * int $movieId, $int $cinemaId	
     * @return int
     */
	public function id(int $movieId, int $cinemaId);

     /**
     * Get theatre list     
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
     public function list();

     /**
     * Get theatre info per id
     *
     * @param  int  $id
     * @return Illuminate\Database\Eloquent\Collection
     */
     public function info(int $id);

     /**
     * Assign cinema a new movie.
     *
     * @param  int  $id
     * @return string
     */
    public function save(int $id, int $movieId);


}