<?php

namespace App\Repositories\Interfaces;

interface ScheduleRepositoryInterface {

	/**
     * Retrieve the shedule list
     *
     * @param  int  $cinemaId, int $movieId
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function list(int $cinemaId, int $movieId);

	/**
     * Retrieve the show date
     *
     * @param  int  $id
     * @return string
     */
	public static function showDateTime(int $id);

}