<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use App\Repositories\Interfaces\CinemaRepositoryInterface;
use App\Models\Cinema;

class CinemaRepository implements CinemaRepositoryInterface
{
	private $model;

	/**
     * Constructor
     *
     * @param  App\Models\Cinema $cinema
     * @return @void
     */
	public function __construct(Cinema $cinema)
	{
		$this->model = $cinema;
	}

	/**
     * Retrieve the cinema list
     *     
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function get(): Collection
	{	
		return $this->model->all();		
	}
}