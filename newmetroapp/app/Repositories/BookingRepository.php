<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BookingRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Booking;
use App\Models\Config;
use App\Models\Schedule;
use Exception;
use Log;

class BookingRepository implements BookingRepositoryInterface
{
	private $model;
    /**
     * Constructor
     *
     * @param  App\Models\Booking $booking
     * @return @void
     */
	public function __construct(Booking $booking)
	{
		$this->model = $booking;
	}

    /**
     * Get booking info per user
     *
     * @param  User $user
     * @return int
     */
	public function list(User $user): Collection 
    {
		
        return $this->model
        ->join('users','users.id','=','bookings.user_id')
        ->join('cinemas','cinemas.id','=','bookings.cinema_id')
        ->join('movies','movies.id','=','bookings.movie_id')
        ->join('schedules','schedules.id','=','bookings.show_time_id')
        ->join('theatres','theatres.id','=','bookings.theatre_id')
        ->select(
            'bookings.id','bookings.reference', 'cinemas.name', 'movies.title'
            , 'bookings.show_date', 'schedules.show_time', 'bookings.number_of_seats'
            , 'bookings.total_cost', 'bookings.created_at','bookings.status'
            , 'theatres.name as theatre_name'            
        )
        ->where('users.id','=',$user->id)
        ->orderBy('id','desc')
        ->get();
	}

    /**
     * Get remaining seat count
     *
     * @param  int $movieId, int $cinemaId, int $showTimeId, int $theatreId, int $maxNumberSeats
     * @return int
     */
    public function remainingSeatCount(
        int $movieId,
        int $cinemaId,
        int $showTimeId,
        int $theatreId,
        int $maxNumberSeats
    ): int
    {
        // get the number of seats used
        $numberOfSeatsUsed = $this->model
            ->where([
                ['movie_id','=',$movieId]
                ,['cinema_id','=',$cinemaId]                
                ,['show_time_id','=',$showTimeId]                
                ,['theatre_id','=',$theatreId]                
                ,['status','=','booked']                
            ])->sum('number_of_seats');

        return $maxNumberSeats - $numberOfSeatsUsed;
    }

    /**
     * Test if cancellation is allowed
     *
     * @param  int $id, User $user
     * @return bool
     */
    public function isCancellationAllowed(int $id, User $user): bool
    {
        // 
        $booking = $this->model->where([
            ['bookings.user_id','=',  $user->id]
            ,['bookings.id', '=', $id]
        ])->first();

        // prevent cancellation of movie an hour before the movie is set to start
        if (\Carbon\Carbon::now()->addHour() >= $booking->show_date)
            return false;
        

        return true;
    }

    /**
     * Save a booking in db
     *
     * @param  int $id, User $user
     * @return App\Models\Booking
     */
    public function info(int $id, User $user): Booking 
    {

        return $this->model
        ->join('users','users.id','=','bookings.user_id')
        ->join('cinemas','cinemas.id','=','bookings.cinema_id')
        ->join('movies','movies.id','=','bookings.movie_id')
        ->join('schedules','schedules.id','=','bookings.show_time_id')
        ->select(
            'bookings.id','bookings.reference', 'cinemas.name', 'movies.title'
            , 'bookings.show_date', 'schedules.show_time', 'bookings.number_of_seats'
            , 'bookings.total_cost', 'bookings.created_at','bookings.status', 'movies.rating'           
        )
        ->where([
            ['users.id','=',$user->id]
            ,['bookings.id', '=', $id]
        ])
        ->first();
    }

    /**
     * Save a booking in db
     *
     * @param  int $id, User $user
     * @return @void
     */
    public function cancel(int $id, User $user, Booking $booking): array
    {        
        try
        {
            // check if movie can be cancelled
            $cancelMovie = $this->isCancellationAllowed(
                $booking->id, $user
            );

            // return an error message if cannot cancel movie
            if (!$cancelMovie) {

                // return array with response
                return [
                    'status'=>false,
                    'message'=>'Cannot cancel movie an hour before it starts.'
                ];
            }

            // set db transaction 
            DB::beginTransaction();

            // cancel a booking
            $updateStatus = $this->model
            ->where('id',$id)
            ->where('user_id', $user->id)
            ->update(['status'=>'cancelled']);

            // commit changes to db
            DB::commit();

            // return array with response
            return [
                'status'=>$updateStatus,
                'message'=>'Movie cancelled for booking reference: ' . $booking->reference
            ];
        }
        catch(Exception $e)
        {
            // log exception
            Log::info($e->getMessage());

            // rollback db transaction
            DB::rollback();

            // return false if update failed
            return [
                'status'=>false,
                'message'=>'Error trying to cancel booking.'
            ];
        }
    }

    /**
     * Save a booking in db
     *
     * @param  array $attributes, User $user 
     * @return array
     */
    public function save(array $attributes, User $user): array
    {
        // start db transaction
        DB::beginTransaction();

        // set the save status
        $saveStatus = true;

        try {

            // save the new booking
            $this->model = new Booking;

            // set input fields to table fields
            $this->model->reference = 'BRN'.mt_rand(1000,10000);        
            $this->model->user_id = $user->id;
            $this->model->cinema_id = $attributes['cinema_id'];
            $this->model->movie_id = $attributes['movie_id'];
            $this->model->show_time_id = $attributes['show_time_id'];           
            $this->model->theatre_id  = $attributes['theatre_id'];           
            $this->model->show_date = $attributes['show_date_and_time'];
            $this->model->number_of_seats = $attributes['seats'];
            $this->model->total_cost = $this->calculateTotalCost(
                $attributes['seats'],
                $attributes['ticket_price']
            );

            // save booking in table
            $status = $this->model->save();

            // commit transaction after save
            DB::commit();

            return [
                "is_saved"=>$status,
                 "message"=>'Saved',
                 "reference"=>$this->model->reference
            ];
        }
        catch(Exception $e){

            // rollback if any errors
            DB::rollback();

            // return the status of the save and the error message
            return [
                "is_saved"=>false,                
                // "message"=>'An error occured saving the request.'
                "message"=>$e->getMessage()
            ];
        } 
    }      

    /**
     * calculate the total cost for the booking
     *
     * @param  int $seats 
     * @return float
     */
    public function calculateTotalCost(int $seats, float $ticketPrice): float
    {
        return $seats * $ticketPrice;
    }
    
}