<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ConfigRepositoryInterface;
use App\Models\Config;

class ConfigRepository implements ConfigRepositoryInterface
{
    private $model;

    /**
     * Constructor
     *
     * @param  App\Models\Config $config
     * @return @void
     */
    public function __construct(Config $config)
    {
        $this->model = $config;
    }

    /**
     * Get value from key value pair table
     *
     * @param  string  $key
     * @return string
     */
    public function value(string $key): string
    {       
        return $this->model->select('value')->where('key', $key)->first()->value;
    }
}

