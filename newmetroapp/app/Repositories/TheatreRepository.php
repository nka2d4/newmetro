<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TheatreRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Theatre;

class TheatreRepository implements TheatreRepositoryInterface
{
	private $model;

    
    /**
     * Constructor for controller
     *
     * @param App\Models\Theatre $theatre
     * @return void
     */
	public function __construct(Theatre $theatre)
	{
		$this->model = $theatre;
	}

    /**
     * Get theatre id     
     * 
     * @param int $movieId, int $cinemaId
     * @return int
     */
	public function id(int $movieId, int $cinemaId): int
	{
		$theatre = $this->model		       
        ->where([
            ['cinema_id',$cinemaId],
            ['movie_id',$movieId],
        ])->first();

        if ($theatre)
			return $theatre->id;

        return 0;
	}

    /**
     * Get theatre list     
     * 
     * @return Illuminate\Database\Eloquent\Collection
     */
	public function list(): Collection
	{
		return $this->model       
        ->join('cinemas','cinemas.id','=','theatres.cinema_id')
        ->join('movies','movies.id','=','theatres.movie_id')
        ->select(
        	'theatres.id','theatres.name as theatre_name', 'theatres.created_at',
        	'theatres.updated_at',
        	'cinemas.name','movies.title', 
        )        
        ->orderBy('theatres.id','asc')
        ->get();
	}

    /**
     * Get theatre info per id
     *
     * @param  int  $id
     * @return App\Models\Theatre
     */
    public function info(int $id): Theatre
    {
        return $this->model
        ->join('cinemas','cinemas.id','=','theatres.cinema_id')
        ->join('movies','movies.id','=','theatres.movie_id')
        ->select(
         'theatres.id','theatres.name as theatre_name', 'theatres.created_at','theatres.updated_at'
         ,'cinemas.name','movies.title', 'movies.id as movie_id'
        )
        ->where([            
            ['theatres.id', '=', $id]
        ])
        ->first();
    }

    /**
     * Assign a cinema a new movie.
     *
     * @param  int  $id
     * @return string
     */
    public function save(int $id, int $movieId): string
    {
        $status = $this->model
        ->where('id', $id)        
        ->update([
           'movie_id'=>$movieId
        ]);

        if ($status)
            return "Theatre movie changed";

        return "Error trying to save new movie for theatre.";
    }
}