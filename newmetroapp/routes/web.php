<?php

use Illuminate\Support\Facades\Route;

// use controllers
use App\Http\Controllers\BookingController;
use App\Http\Controllers\TheatreController;
use App\Http\Controllers\CinemaController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\ConfigController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//
// Landing page route
// 
Route::get('/', function () {
    return view('welcome');
});

//
// Dashboard route
// 
Route::middleware(['auth'])->get('/dashboard', function () {
	return view('dashboard');
})->name('dashboard');

//
// Additional routes to controllers
//
Route::get('schedule/list/{cinema_id}/{movie_id}',  [ScheduleController::class, 'list']);
Route::get('theatre/id/{movie_id}/{cinema_id}',  [TheatreController::class, 'id']);
Route::get('config/maxseats',  [ConfigController::class,'maxseats']);
Route::get(
	'booking/remainingseats/{movie_id}/{cinema_id}/{show_time_id}/{theatre_id}/{max_number_seats}',	
	[BookingController::class,'remainingseats']
);

//
// Route action with auth
//
Route::post('booking', [ConfigController::class,'store'])->middleware('auth');
//
// Controller routes
//
Route::resources(['booking' => BookingController::class,]);
Route::resources(['theatre' => TheatreController::class,]);
Route::resources(['cinema' => CinemaController::class,]);
Route::resources(['movie' => MovieController::class,]);
Route::resources(['schedule' => ScheduleController::class,]);
Route::resources(['config' => ConfigController::class,]);
