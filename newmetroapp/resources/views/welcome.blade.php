<x-general-layout>
        
        <!-- Welcome screen -->
        <div class="w-full h-screen bg-center bg-no-repeat bg-cover" style="background-image: url('img/theatre.jpg');">
            <div class="w-full h-screen bg-opacity-50 bg-black flex justify-center items-center">
                <div class="mx-4 text-center text-white">
                    <h1 class="font-bold text-6xl mb-4">Welcome to New Metro!</h1>
                    <h2 class="font-bold text-3xl mb-12">The best show stopper cinema in town.</h2>
                    <div>
                        <a href="/booking/create" class="bg-blue-500 rounded-md font-bold text-white text-center px-4 py-3 transition duration-300 ease-in-out hover:bg-blue-600 mr-2">
                            Book now
                        </a>
                        <a href="/register" class="bg-red-500 rounded-md font-bold text-white text-center px-4 py-3 transition duration-300 ease-in-out hover:bg-red-600 ml-2 mr-2">
                            Register
                        </a>
                        <a href="/login" class="bg-gray-500 rounded-md font-bold text-white text-center px-4 py-3 transition duration-300 ease-in-out hover:bg-white-600 ml-2">
                            Login
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

</x-general-layout>