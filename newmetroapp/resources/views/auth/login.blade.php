<x-guess-layout>

<!-- Root element for center items -->
<div class="flex flex-col h-screen bg-white-100">	
    <!-- Auth Card Container -->
    <div class="grid place-items-center mx-2 my-20 sm:my-auto bg-white">
        <!-- Auth Card -->
        <div class="w-11/12 p-12 sm:w-8/12 md:w-6/12 lg:w-5/12 2xl:w-4/12 
            px-6 py-10 sm:px-10 sm:py-6 
            bg-white rounded-lg shadow-md lg:shadow-lg">

            <form class="mt-10" action="/login" method="POST">
            	@csrf
                <!-- Email Input -->
                <label for="email" class="block text-xs font-semibold text-gray-600 uppercase">E-mail</label>
                <input id="email" type="email" name="email" placeholder="e-mail address" autocomplete="email"
                    class="block w-full py-3 px-1 mt-2 
                    text-gray-800 appearance-none 
                    border-b-2 border-gray-100
                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
                    required />

                <!-- Password Input -->
                <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Password</label>
                <input id="password" type="password" name="password" placeholder="password" autocomplete="current-password"
                    class="block w-full py-3 px-1 mt-2 mb-4
                    text-gray-800 appearance-none 
                    border-b-2 border-gray-100
                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
                    required />

                <!-- Auth Buttton -->
                <button type="submit"
                    class="w-full py-3 mt-10 bg-gray-800 rounded-sm
                    font-medium text-blue-400 uppercase
                    focus:outline-none hover:bg-gray-700 hover:shadow-none">
                    Login
                </button>

                <!-- Browse movies or create an account-->
                <div class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center text-gray-800">
                    <a href="/booking/create" class="flex-2 underline">
                        Make booking
                    </a>

                    <a href="/" class="flex-1 text-gray-800 underline text-md mx-4 my-1 sm:my-auto">
                        Home
                    </a>
        
                    <a href="/register" class="flex-2 underline text-gray-800">
                        Create an Account
                    </a>
                </div>

				<!-- error handling when user registers -->
                <div class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center">	                
					@if ($errors->any())
						<br>
						<div class="text-center text-red-600">
							<ul>
								@foreach($errors->all() as $error)
									<li>{{$error}}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>

            </form>
        </div>
    </div>
</div>	

</x-guess-layout>