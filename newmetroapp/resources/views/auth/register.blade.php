<x-guess-layout>

	<!-- Root element for center items -->
	<div class="flex flex-col h-screen bg-white-100">
	    <!-- Auth Card Container -->
	    <div class="grid place-items-center mx-2 my-20 sm:my-auto bg-white">
	        <!-- Auth Card -->
	        <div class="w-11/12 p-12 sm:w-8/12 md:w-6/12 lg:w-5/12 2xl:w-4/12 
	            px-6 py-10 sm:px-10 sm:py-6 
	            bg-white rounded-lg shadow-md lg:shadow-lg">

	           	           
	            <form class="mt-10" action="/register" method="POST">
	            	@csrf
	                <!-- Name input-->
	                <label for="name" class="block text-xs font-semibold text-gray-600 uppercase">Name</label>
	                <input id="name" type="input" name="name" placeholder="Name" autocomplete="Name"
	                    class="block w-full py-3 px-1 mt-2 
	                    text-gray-800 appearance-none 
	                    border-b-2 border-gray-100
	                    focus:text-gray-500 focus:outline-none focus:border-gray-200"                    
	                    value="{{ old('name') }}" autofocus/> 
	                <!-- Email Input -->
	                <label for="email" class="block text-xs font-semibold text-gray-600 uppercase">E-mail</label>
	                <input id="email" type="email" name="email" placeholder="e-mail address" autocomplete="email"
	                    class="block w-full py-3 px-1 mt-2 
	                    text-gray-800 appearance-none 
	                    border-b-2 border-gray-100
	                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
	                    value="{{ old('email') }}" />

	                <!-- Password Input -->
	                <label for="password" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Password</label>
	                <input id="password" type="password" name="password" placeholder="password"
	                    class="block w-full py-3 px-1 mt-2 mb-4
	                    text-gray-800 appearance-none 
	                    border-b-2 border-gray-100
	                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
	                    />
	                <!-- Password confirmation -->
	                <label for="password_confirmation" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Password confirmation</label>
	                <input id="password_confirmation" type="password" name="password_confirmation" placeholder="password confirmation"
	                    class="block w-full py-3 px-1 mt-2 mb-4
	                    text-gray-800 appearance-none 
	                    border-b-2 border-gray-100
	                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
	                    />

	                <!-- Auth Buttton -->
	                <button
	                    class="w-full py-3 mt-10 bg-gray-800 rounded-sm
	                    font-medium text-blue-400 uppercase
	                    focus:outline-none hover:bg-gray-700 hover:shadow-none">
	                    Register
	                </button>

	                <!-- Browse movies or create an account-->
	                <div class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center text-gray-800">
	                    <a href="/" class="flex-2 underline">
	                        Browse movies
	                    </a>

	                    <p class="flex-1 text-gray-500 text-md mx-4 my-1 sm:my-auto">
	                        or
	                    </p>
	        
	                    <a href="/login" class="flex-2 underline text-gray-800">
	                        Already a user?
	                    </a>
	                </div>

					<!-- error handling when user registers -->
	                <div class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center">	                
						@if ($errors->any())
							<br>
							<div class="text-left"> Registration error!</div>
							<br>
							<div class="text-center text-red-600">
								<ul>
									@foreach($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
								</ul>
							</div>
						@endif
					</div>

	            </form>
	        </div>
	    </div>
	</div>	


</x-guess-layout>