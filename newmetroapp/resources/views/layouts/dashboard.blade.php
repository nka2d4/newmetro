<!-- header component -->
<x-header-layout>
</x-header-layout>


<body>	

	<form method='POST' action="{{ route('logout') }}">
	@csrf
		<!-- TOP Nav component -->
		<div class="flex-1 flex flex-col">
			<nav class="px-4 flex justify-between bg-white h-16 border-b-2">

				<!-- Left bar -->
				<ul class="flex items-center">
					<!-- Logo here -->
					<li class="h-6 w-6">
						
					</li>
				</ul>

				<!-- Centre bar -->
				<ul class="flex items-center">				
					<li>
						<h1 class="pl-8 lg:pl-0 text-gray-700">New Metro</h1>
					</li>
				</ul>
				<!-- Right bar -->
				<ul class="flex items-center">

					<li class="pr-6">
						<a href='/booking/create'>Make booking</a>
					</li>
					<li class="pr-6">
						<a href="{{ route('logout') }}" 
							onclick="event.preventDefault();this.closest('form').submit();">
							logout
						</a>
					</li>

					

				</ul>
			</nav>
		</div>
	</form>

	<div>
		{{ $slot }}
	</div>



</body>
</html>