<!-- header component -->
<x-header-layout>
</x-header-layout>

<body>
	<div>
		<ul>
			<li>
				<form method="POST" action="{{ route('logout')}}">
					@csrf

					<a href="{{ route('logout') }}" onclick="event.preventDefault();this.closest('form').submit();">
						logout
					</a>
				</form>
			</li>
		</ul>
	</div>
	
	<div>
		Nav for app layout
	</div>

	<div>
		{{ $slot }}
	</div>

</body>
</html>