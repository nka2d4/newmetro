<x-general-layout>

	<!-- TOP Nav component -->
	<div class="flex-1 flex flex-col">
		<nav class="px-4 flex justify-between bg-white h-16 border-b-2">

			<!-- Left bar -->
			<ul class="flex items-center">
				<!-- Logo here -->
				<li class="h-6 w-6">
					
				</li>
			</ul>

			<!-- Centre bar -->
			<ul class="flex items-center">				
				<li>
					<h1 class="pl-8 lg:pl-0 text-gray-700">New Metro</h1>
				</li>
			</ul>

			<!-- Right bar -->
			<ul class="flex items-center">

				<li class="pr-6">


				</li>

				<li class="h-10 w-10">

				</li>

			</ul>

		</nav>
	</div>

	<script type="text/javascript">
		$( document ).ready(function() {

			var movieList, maxSeats;

			function loadCinemaSelect()
			{
				var jqxhr = $.getJSON( "/cinema", function() {
				  // console here
				})
				.done(function(result) {				    

				    var $dropdown = $("#cinema_id");

				    $dropdown
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="0">Choose a cinema...</option>')
				    .val('0');

					$.each(result, function() {
					    $dropdown.append($("<option />").val(this.id).text(this.name));
					});
				})
				.fail(function() {
				  	var $error = $("#error_div");
				    $error.text = 'Error loading cinema div.';
				    $error.toggleClass( 'hidden', false );			    				    
				})
				.always(function() {
				    
				});
			}

			function loadMovieSelect(cinemaId)
			{
				
				var jqxhr = $.getJSON( "/movie/"+cinemaId, function() {
				  
				})
				.done(function(result) {
				    

				    var $dropdown = $("#movie_id");

				    $dropdown
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="0">Choose a movie...</option>')
				    .val('0');

					$.each(result, function() {
					    $dropdown.append($("<option />").val(this.movie_id).text(this.title));
					});

					// set global variable for movie list
					movieList = result;
				})
				.fail(function() {
				  	var $error = $("#error_div");
				    $error.text = 'Error loading movie div.';
				    $error.toggleClass( 'hidden', false );				    
				})
				.always(function() {
				    
				});
			}

			function loadScheduleSelect(cinemaId, movieId)
			{				
				var jqxhr = $.getJSON( "/schedule/list/"+cinemaId+'/'+movieId, function() {
				  
				})
				.done(function(result) {
				    
				    var $dropdown = $("#show_time_id");

				    $dropdown
				    .find('option')
				    .remove()
				    .end()
				    .append('<option value="0">Choose a time...</option>')
				    .val('0');

					$.each(result, function() {
					    $dropdown.append($("<option />").val(this.id).text(this.show_time));
					});
				})
				.fail(function() {
				  	var $error = $("#error_div");
				    $error.text = 'Error loading schedule div.';
				    $error.toggleClass( 'hidden', false );				    
				})
				.always(function() {
				    
				});
			}

			function getMaxSeats()
			{
				var jqxhr = $.getJSON( "/config/maxseats", function() {
				  
				})
				.done(function(result) {				    
				    if (result)
				    {
				    	$('input#max_number_seats').val(result.max_seats);
				    	getTheatreId(result.max_seats);
				    }
				})
				.fail(function() {
				  	var $error = $("#error_div");
				    $error.text = 'Error fetching max seats.';
				    $error.toggleClass( 'hidden', false );
				})
				.always(function() {
				    
				});
			}

			function getTheatreId(maxSeats)
			{
				var cinemaId = $( "select#cinema_id option:selected" ).val(),
				movieId = $( "select#movie_id option:selected" ).val();

				var jqxhr = $.getJSON( "/theatre/id/"+movieId+"/"+cinemaId, function() {
				  
				})
				.done(function(result) {				    
				    if (result)
				    {
				    	$('input#theatre_id').val(result.theatre_id);
				    	getRemainingSeats(maxSeats, result.theatre_id);
				    }
				})
				.fail(function() {
				  	var $error = $("#error_div");
				    $error.text = 'Error fetching max seats.';
				    $error.toggleClass( 'hidden', false );
				})
				.always(function() {
				    
				});
			}

			function getRemainingSeats(maxSeats, theatreId)
			{				
				// $movieId, $cinemaId, $showTimeId, $theatreId, $maxNumberSeats
				var cinemaId = $( "select#cinema_id option:selected" ).val(),
				movieId = $( "select#movie_id option:selected" ).val(),
				showTimeId = $( "select#show_time_id option:selected" ).val();

				var jqxhr = $.getJSON( 
					"/booking/remainingseats/"+movieId
					+ "/" + cinemaId + 
					"/" + showTimeId +
					"/" + theatreId +
					"/" + maxSeats, function() {
				  
				})
				.done(function(result) {				    
				    if (result)
				    {						
				    	$('input#seats_remaining').val(result.remaining_seat_count);

				    	var $dropdown = $("#seats");

					    $dropdown
					    .find('option')
					    .remove()
					    .end()
					    .append('<option value="0">Choose number of seats...</option>')
					    .val('0');

						for (i=1; i <= result.remaining_seat_count; i++) {
						    $dropdown.append($("<option />").val(i).text(i));
						}

						$( "#make_booking" ).toggleClass( 'hidden', false );

						// show error message if movie sold out.
						if (result.remaining_seat_count==0)
						{
							var $error = $("#error_div");
							$error.toggleClass( 'hidden', false );							
					    	$error.text('Movie sold out.');
					    	$( "#make_booking" ).toggleClass( 'hidden', true );
				    	}
				    }
				})
				.fail(function() {
				  	var $error = $("#error_div");
				    $error.text = 'Error fetching max seats.';
				    $error.toggleClass( 'hidden', false );				    
				})
				.always(function() {
				    
				});
			}

			function setupMovieImgDiv(movies, movieId)
			{
				var $movieImage = $("img#movie_img"),
				$movieTitle = $('div#movie_title'),
				$movieRating = $('div#movie_rating');
				
				$.each(movies, function() {					
					if (this.movie_id==movieId)
					{					
						$movieImage.attr(
							'src',
							'https://image.tmdb.org/t/p/w200/' + this.poster_path
						);
						$movieTitle.text(this.title);
						$movieRating.text('Rating: '+ this.rating);
					}
				});
			}

			function resetSelectList(elementIdToReset, message, selectedElementId)
			{
				var selectValue = $( "select#" + selectedElementId +" option:selected" ).val();

				if (selectValue == 0)
				{
					var $dropdown = $("#" + elementIdToReset);

					$dropdown
					    .find('option')
					    .remove()
					    .end()
					    .append('<option value="0">Choose a '+ message +'...</option>')
					    .val('0');
				}
			}

			function resetMovieThumbnail()
			{
				$( "#movie_img" ).attr('src','');
				$( "#movie_title" ).text('');
				$( "#movie_rating" ).text('');
			}

			function setupMovieSelect()
			{
				var cinemaId = $( "select#cinema_id option:selected" ).val();

				resetSelectList('movie_id', 'movie', 'cinema_id');
				resetSelectList('show_time_id', 'show time', 'movie_id');
				
				resetMovieThumbnail();

				loadMovieSelect(cinemaId);
			}

			function setupScheduleSelect(movies)
			{
				var cinemaId = $( "select#cinema_id option:selected" ).val(),
				movieId = $( "select#movie_id option:selected" ).val();

				// resetSelectList('show_time_id', 'show time', 'movie_id');
				
				// resetMovieThumbnail();
				
				loadScheduleSelect(cinemaId, movieId);
				setupMovieImgDiv(movies, movieId);
			}

			function setupNumberSeatsSelectBox(maxSeats)
			{			
				getMaxSeats();
			}

			function reset()
			{
				var $error = $("#error_div");
				$error.toggleClass( 'hidden', true );
			}
		
			loadCinemaSelect();

			$( "select#cinema_id" ).change(function() {
				reset();
			  	setupMovieSelect();
			});

			$( "select#movie_id" ).change(function() {				
				reset()
			  	setupScheduleSelect(movieList);
			});

			$( "select#show_time_id" ).change(function() {
				reset();
			  	setupNumberSeatsSelectBox(maxSeats);
			});

			$( "#seats" ).change(function() {			  
				reset();
				$( "#make_booking" ).toggleClass( 'hidden', false );
			});

	}); // end ready
	</script>

	<!-- Root element for center items -->
	<div class="flex flex-col h-screen bg-white-100">	
	    <!-- Parent Card Container -->
	    <div class="grid place-items-center mx-2 my-20 sm:my-auto bg-white">
	        <!-- ChildCard -->
	        <div class="w-11/12 p-12 sm:w-8/12 md:w-6/12 lg:w-5/12 2xl:w-4/12 
	            px-6 py-10 sm:px-10 sm:py-6 
	            bg-white rounded-lg shadow-md lg:shadow-lg">
	            <!-- form -->
	            <form class="mt-10" action="/booking" method="POST">
	            	
	            	<!-- declare CSRF token -->
	            	@csrf
	                <!-- Select cinema -->
	                <label for="cinema_id" class="block text-xs font-semibold text-gray-600 uppercase">Cinema</label>

	            	<select id="cinema_id" type="select" name="cinema_id" placeholder="Choose a cinema" 
		                    class="block w-full py-3 px-1 mt-2 
		                    text-gray-800 appearance-none 
		                    border-b-2 border-gray-100
		                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
		                    required
		                    ">
		                    	<option value='0'>Choose a Cinema</option>
		            </select>
	                
					<br>

					<label for="movie_button_id" class="block text-xs font-semibold text-gray-600 uppercase">Movie</label>	                
	                <div class="flex flex-nowrap">
		                
		                <select id="movie_id" type="select" name="movie_id" 
	                		placeholder="Choose a movie" 
		                    class="block w-full py-3 px-1 mt-2 
		                    text-gray-800 appearance-none 
		                    border-b-2 border-gray-100
		                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
		                    required
		                    >
		                    	<option value='0'>Choose a Movie</option>
		                    	<!-- fetch cinemas from model -->								
		                </select>

	            	</div>
	                <div id='movie_thumnail' class="pr-4 pl-4 border-dashed border-4 border-blue-500 text-center">
                		<img id='movie_img' class='mx-auto text-center' src="" /> 
                		<br>
                		<div id='movie_title'></div>
                		<br>
                		<div id='movie_rating'></div>	                		
        			</div>

	            	<br>

	                <!-- Select show time -->
	                <label for="show_time_id" class="block text-xs font-semibold text-gray-600 uppercase">Show time</label>

		                <select id="show_time_id" type="select" name="show_time_id"
		                    class="block w-full py-3 px-1 mt-2 
		                    text-gray-800 appearance-none 
		                    border-b-2 border-gray-100
		                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
		                    required>
		                    	<option value='0'>Choose a show time</option>
		                </select>
	                
	                <br>

					<!-- Select seats -->
	                <label for="seats" class="block text-xs font-semibold text-gray-600 uppercase">Number of seats</label>

	                <select id="seats" type="select" name="seats"
	                    class="block w-full py-3 px-1 mt-2 
	                    text-gray-800 appearance-none 
	                    border-b-2 border-gray-100
	                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
	                    required>
			                <option value='0'>Choose number of seats</option>
			        </select>
	                

	                <div id='error_div' class="alert alert-danger hidden" role="alert"></div>        

	                <input type='hidden' id='max_number_seats' value=''>
	                <input type='hidden' id='seats_remaining' value=''>
		            <input type="hidden" name="theatre_id" id="theatre_id" value="" />


	                <!-- Submit Buttton -->
	                <button type="submit" id="make_booking" name="make_booking" value='1'
	                    class="w-full py-3 mt-10 bg-gray-800 rounded-sm hidden
	                    font-medium text-blue-400 uppercase
	                    focus:outline-none hover:bg-gray-700 hover:shadow-none">
	                    Place booking
	                </button>


		            
		            

	                <!-- Browse movies or create an account-->
	                <div class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center text-gray-800">
	                    <a href="/login" class="flex-2 underline">
	                        Login
	                    </a>

	                    <a href="/" class="flex-1 text-gray-800 underline text-md mx-4 my-1 sm:my-auto">
	                        Home
	                    </a>
	        
	                    <a href="/register" class="flex-2 underline text-gray-800">
	                        Create an Account
	                    </a>
	                </div>

					<!-- error handling when user registers -->
	                <div class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center">	    

	                	</div>
						@if ($errors->any())
							<br>
							<div class="alert alert-danger" role="alert">							
								<ul>
									@foreach($errors->all() as $error)
										<li>{{$error}}</li>
									@endforeach
								</ul>
							</div>
						@endif

						@isset($status)
							@if (!$status['is_saved'])
							<div class="alert alert-danger" role="alert">
								{{$status['message']}}
								
							<br>
							<a href="/booking/create" 
								class="sm:flex sm:flex-wrap mt-8 sm:mb-4 text-sm text-center text-gray-800 underline">	
							    Refresh page
	                    	</a>
							</div>
							@endif 
						@endisset
					</div>

	            </form>
	        </div>
	    </div>
	</div>	

</x-general-layout>