<!-- header component -->
<x-header-layout>
</x-header-layout>

<x-dashboard-layout>

<div class="container-fluid">
  
  <br><br>
  Your bookings
  <br><br>  

  @if(session('result'))
    @if(session('result')['status'])
      <p class="card-text text-success">
        {{session('result')['message']}}.
      </p>
      <br>
    @endif
  @endif

  @if(session('result'))
    @if(!session('result')['status'])
      <p class="card-text text-error">
        {{session('result')['message']}}.
      </p>
      <br>
    @endif
  @endif
  
  <table class="table table-sm">
    <thead>
      <tr>        
        <th scope="col">Reference</th>
        <th scope="col">Cinema</th>
        <th scope="col">Movie</th>
        <th scope="col">Show Time</th>
        <th scope="col">Seats</th>
        <th scope="col">Total cost</th>
        <th scope="col">Date created</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach ($bookings as $booking) 
        <tr>          
          <th scope="row">{{$booking->reference}}</th>
          <td>{{$booking->name}} ({{$booking->theatre_name}})</td>
          <td>{{$booking->title}}</td>
          <td>{{$booking->show_date}}</td>
          <td>{{$booking->number_of_seats}}</td>
          <td>R{{$booking->total_cost}}</td>
          <td>{{$booking->created_at}}</td>
          <td>
              @if ($booking->status=='booked')
                <span class="badge badge-success">{{$booking->status}}</span>
              @else 
                <span class="badge badge-secondary">{{$booking->status}}</span>
              @endif
          </td>          
          <td>
            @if ($booking->status=='booked')
              <a class='btn-sm btn-danger' role='button' href='booking/{{$booking->id}}/edit'>Cancel</a>
            @endif
          </td>
          <td></td>
        </tr>        
      @endforeach      
    </tbody>    
  </table>

</div>

</x-dashboard-layout>