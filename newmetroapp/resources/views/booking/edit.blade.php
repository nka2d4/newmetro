<!-- header component -->
<x-header-layout>
</x-header-layout>

<x-dashboard-layout>

<div class="container-fluid">
  
  <br><br>  
  <form action="/booking/{{$bookingInfo->id}}" method="POST">
    @method('PUT')
    @csrf

		<div class="card text-center">
		  <div class="card-header">
		    Cancel booking
		  </div>
		  <div class="card-body">	    
		    <p class="card-text font-weight-bold">Title</p>
		    <p class="card-text">{{$bookingInfo->title}}</p>
		    <br>
		    <p class="card-text font-weight-bold">Movie rating:</p>
		    <p class="card-text">{{$bookingInfo->rating}}</p>
		    <br>
		    <p class="card-text font-weight-bold">Number of seats booked</p>
		    <p class="card-text">{{$bookingInfo->number_of_seats}}</p>
		    <br>
		    <p class="card-text font-weight-bold">Total cost</p>
		    <p class="card-text">{{$bookingInfo->total_cost}}</p>
		    <br>

		    <p class="card-text font-weight-bold">Show time</p>
		    <p class="card-text">
		    	{{$bookingInfo->show_date}}	    	
		    </p>
		    <br>
		    @isset($result)		    
		    	@if (!$result["status"])
		    		<p class="card-text text-danger">{{$result["message"]}}</p>
		    	@endif
		    @endisset		    
		  </div>
		  <div class="card-footer text-muted">

		  	
		    <button class="btn btn-primary">Confirm</button>
		    
		    <a href="\booking" class="btn btn-info">Return</a>
		  </div>
		</div>
	</form>
</div>

</x-dashboard-layout>