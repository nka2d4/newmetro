<!-- header component -->
<x-header-layout>
</x-header-layout>

<x-crud-layout>
  <div class="container-fluid">
    
    <br><br>
    Theatre admin
    <br><br>    
    Assign a theatre a new movie.
    <br><br>  

    @if(session('status'))
    <p class="card-text text-success">
      {{session('status')}}.
    </p>
    <br>
    @endif
    
    <table class="table table-sm">
      <thead>
        <tr>        
          <th scope="col">ID</th>
          <th scope="col">Cinema</th>
          <th scope="col">Movie</th>        
          <th scope="col">Theatre</th>
          <th scope="col">Updated</th>
          <th scope="col">Date created</th>        
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($theatres as $theatre) 
          <tr>          
            <th scope="row">{{$theatre->id}}</th>
            <td>{{$theatre->name}}</td>
            <td>{{$theatre->title}}</td>
            <td>{{$theatre->theatre_name}}</td>          
            <td>{{$theatre->updated_at}}</td>
            <td>{{$theatre->created_at}}</td>                    
            <td>            
                <a class='btn-sm btn-danger' role='button' href='theatre/{{$theatre->id}}/edit'>Change movie</a>
              
            </td>          
          </tr>        
        @endforeach      
      </tbody>    
    </table>

  </div>
</x-crud-layout>