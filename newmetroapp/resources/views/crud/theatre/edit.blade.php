<!-- header component -->
<x-header-layout>
</x-header-layout>

<x-crud-layout>
  <div class="container-fluid">
    
  
  <br><br>  
  <form action="/theatre/{{$theatreInfo->id}}" method="POST">
    @method('PUT')
    @csrf

		<div class="card text-center">
		  <div class="card-header">
		    Change movie for a theatre
		  </div>
		  <div class="card-body">	    
		    <p class="card-text font-weight-bold">Cinema</p>
		    <p class="card-text">{{$theatreInfo->name}}</p>
		    <br>
		    <p class="card-text font-weight-bold">Theatre</p>
		    <p class="card-text">{{$theatreInfo->theatre_name}}</p>
		    <br>
		    <p class="card-text font-weight-bold">Movie:</p>
		    <p class="card-text">
		    	@isset($movies)
			    	<select id='movie_id' name='movie_id'>
			    		@foreach($movies as $movie)
			    		<option value='{{$movie->id}}' 
			    			@if ($theatreInfo->movie_id == $movie->id)
			    				selected
			    			@endif
			    			>{{$movie->title}}</option>
			    		@endforeach
			    	</select>
		    	@endisset($movies)
		    </p>

		    <br>		    
		  </div>

		  <div class="card-footer text-muted">
		  	
		    	<button class="btn btn-primary">Confirm</button>
		    
		    	<a href="\theatre" class="btn btn-info">Return</a>
		  </div>
		</div>
	</form>   

  </div>
</x-crud-layout>