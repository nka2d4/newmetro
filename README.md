# Introduction

The Movie Booking system was developed with Laravel 8 on Ubuntu. It uses Docker with Laravel Sail.

## Installation using Ubuntu

### Dependancies

In order to run Laravel's Docker **Sail** ensure these are installed.

  *  Docker
  *  php 7.3 or higher
  *  php-xml
  *  php-mbstring
  *  composer v1.10 or higher

### Install docker

  1. **[Install docker for Ubuntu](https://docs.docker.com/engine/install/ubuntu/)**

    See header “Install using the repository”

### Post docker install configs

  **Please note this is very important to configure for Sail to work.**

  1. **Create a docker group:**

    ```
    $ sudo groupadd docker
    ```

  2. **Add your user to the docker group:**

    ```
    $ sudo usermod -aG docker $USER
    ```

  3. **Activate changes to the group run the following command:**

    ```
    $ newgrp docker 
    ```

    if this does not help. Log out and back.

    If testing on a virtual machine, it may be necessary to restart the virtual machine for changes to take effect.

### PHP setup

  1. Ensure you have php 7.3 and higher installed
```
    $ php -v
```

### PHP Extensions
  1. If you dont have PHP installed then please install it.

  2. Ensure you have the php-xml and php-mbstring extension, if not  run the following commands:
```
    $ sudo apt-get update
    $ sudo apt install php-xml
    $ sudo apt-get install php-mbstring
```

### Clone the git repo!

  1. $ git clone https://bitbucket.org/nka2d4/newmetro.git

  2. Setup dependancies
```
    $ cd newmetro/newmetroapp/

    $ composer install
```

### Start your container!

  Ensure you are in the root of the project.

    $ cd newmetro/newmetroapp/

    $ ./vendor/bin/sail up

### Open the website

  Open the website [http://localhost](http://localhost)

### Open the mailer (laravel mailhog)

  Open the website [http://localhost:8025/](http://localhost:8025/)

### Open CRUD admin 

  [Crud Theatre Admin](http://localhost/theatre)

  TODO: Create authentication for CRUD.

### Laravel Sail

  Sail comes with NPM, Composer, GIT, PHP Artisan and PHP Unit. Running commands is easy.
  To run the above commands simply start with 
```
  ./vendor/bin/sail <<command here>>
```
### MySql

  1. **DB Migrations**

    In the root directory of the solution to reset the DB run:

    
    $ ./vendor/bin/sail php artisan migrate:reset

    $ ./vendor/bin/sail php artisan migrate --seed
    

  2. **View database**

    1. download table plus editor or any of your choice
    2. IP: 127.0.0.1
    3. PORT: 3306
    4. User: root
    5. password: <<leave blank>>
    6. DB: newmetroapp

### Website screenshots

#### Welcome page

![Home](readme_img/home.png)

#### Booking page

![Booking](readme_img/booking.png)

#### Mange booking page

![Manage booking](readme_img/manage.png)

#### Cancel booking page

![Cancel booking](readme_img/cancel.png)

#### CRUD manage theatre

![Theatre manage](readme_img/theatre.png)

#### CRUD change move for theatre

![Change theatre movie](readme_img/theatre2.png)